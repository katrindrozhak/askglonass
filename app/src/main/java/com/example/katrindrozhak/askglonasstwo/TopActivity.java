package com.example.katrindrozhak.askglonasstwo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopActivity extends Activity {

    ListView listView;
    List<StateInfo> arrayListStateInfo;
    TableAdapter tableAdapter;
    Call<JsonSchemaInfo> call;
    // Объект Runnable, который запускает метод для выполнения задач
    // в фоновом режиме (таймер)
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            fillTable();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top);

        listView = (ListView) findViewById(R.id.listViewTop);

        arrayListStateInfo = new ArrayList<>();
        tableAdapter = new TableAdapter(arrayListStateInfo);

        listView.setAdapter(tableAdapter);

        fillTable();
    }

    private void fillTable() {

        getListData();

        call.enqueue(new Callback<JsonSchemaInfo>() {
            @Override
            public void onResponse(Call<JsonSchemaInfo> call, Response<JsonSchemaInfo> response) {

                if (response.isSuccessful()) {

                    JsonSchemaInfo listObject = response.body();

                    arrayListStateInfo = listObject.getObjects();
                    // присваивает новый массив данных адаптеру
                    tableAdapter.setStateInfoList(arrayListStateInfo);
                    // уведомляет список об изменении данных для обновления списка на экране.
                    tableAdapter.notifyDataSetChanged();

                } else
                    Toast.makeText(getApplicationContext(), "Response code is "
                            + response.code(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<JsonSchemaInfo> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Failure! " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        // объект должен быть запущен после задержки в 5 секунд
        new Handler().postDelayed(runnable, 5 * 1000);
    }

    public void getListData() {
        Map<String, String> data = new HashMap<>();
        data.put(Const.FUNK_KEY, Const.FUNK_VALUE);
        data.put(Const.KEY_FUEL, Const.VALUE_FUEL);
        data.put(Const.UID_KEY, Const.UID_VALUE);
        data.put(Const.OUT_KEY, Const.OUT_VALUE);

        //Получаем наш список данных о пользователе.
        call = new RestManager().getServerApi().getData(data);
    }
}
