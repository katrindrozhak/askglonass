
package com.example.katrindrozhak.askglonasstwo;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class JsonSchemaInfo {

    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("objects")
    @Expose
    private List<StateInfo> objects = new ArrayList<StateInfo>();

    /**
     * 
     * @return
     *     The result
     */
    public String getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * 
     * @return
     *     The objects
     */
    public List<StateInfo> getObjects() {
        return objects;
    }

    /**
     * 
     * @param objects
     *     The objects
     */
    public void setObjects(List<StateInfo> objects) {
        this.objects = objects;
    }

}
