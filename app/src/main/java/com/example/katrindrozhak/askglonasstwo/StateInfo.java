
package com.example.katrindrozhak.askglonasstwo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class StateInfo {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("model_type")
    @Expose
    private String modelType;
    @SerializedName("garagenum")
    @Expose
    private String garagenum;
    @SerializedName("statenum")
    @Expose
    private String statenum;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("course")
    @Expose
    private String course;
    @SerializedName("speed")
    @Expose
    private String speed;
    @SerializedName("fuel")
    @Expose
    private String fuel;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The model
     */
    public String getModel() {
        return model;
    }

    /**
     * 
     * @param model
     *     The model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * 
     * @return
     *     The modelType
     */
    public String getModelType() {
        return modelType;
    }

    /**
     * 
     * @param modelType
     *     The model_type
     */
    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    /**
     * 
     * @return
     *     The garagenum
     */
    public String getGaragenum() {
        return garagenum;
    }

    /**
     * 
     * @param garagenum
     *     The garagenum
     */
    public void setGaragenum(String garagenum) {
        this.garagenum = garagenum;
    }

    /**
     * 
     * @return
     *     The statenum
     */
    public String getStatenum() {
        return statenum;
    }

    /**
     * 
     * @param statenum
     *     The statenum
     */
    public void setStatenum(String statenum) {
        this.statenum = statenum;
    }

    /**
     * 
     * @return
     *     The time
     */
    public String getTime() {
        return time;
    }

    /**
     * 
     * @param time
     *     The time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * 
     * @return
     *     The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     * 
     * @param lat
     *     The lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * 
     * @return
     *     The lon
     */
    public String getLon() {
        return lon;
    }

    /**
     * 
     * @param lon
     *     The lon
     */
    public void setLon(String lon) {
        this.lon = lon;
    }

    /**
     * 
     * @return
     *     The course
     */
    public String getCourse() {
        return course;
    }

    /**
     * 
     * @param course
     *     The course
     */
    public void setCourse(String course) {
        this.course = course;
    }

    /**
     * 
     * @return
     *     The speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * 
     * @param speed
     *     The speed
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    /**
     * 
     * @return
     *     The fuel
     */
    public String getFuel() {
        return fuel;
    }

    /**
     * 
     * @param fuel
     *     The fuel
     */
    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

}
