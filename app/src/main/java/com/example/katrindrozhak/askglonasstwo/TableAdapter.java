package com.example.katrindrozhak.askglonasstwo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class TableAdapter extends BaseAdapter {

    public List<StateInfo> stateInfoList;

    public void setStateInfoList(List<StateInfo> stateInfoList) {
        this.stateInfoList = stateInfoList;
    }

    public TableAdapter(List<StateInfo> data) {
        stateInfoList = data;
    }

    @Override
    public int getCount() {
        return stateInfoList.size();
    }

    @Override
    public Object getItem(int position) {
        return stateInfoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // convertView уже созданное ранее View, но неиспользуемое в данный момент
        // оно значительно ускоряет работу приложения, т.к. не надо прогонять inflate лишний раз.
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item, parent, false);
        }

        StateInfo stateInfo = stateInfoList.get(position);

        ((TextView) convertView.findViewById(R.id.itemStatenum)).setText(stateInfo.getStatenum());

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        try {
            String formattedTime = formatter.format(parser.parse(stateInfo.getTime()));
            ((TextView) convertView.findViewById(R.id.itemTime)).setText(formattedTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ((TextView) convertView.findViewById(R.id.itemFuel)).setText(stateInfo.getFuel());

        ((TextView) convertView.findViewById(R.id.itemSpeed)).setText(stateInfo.getSpeed());

        return convertView;
    }
}
