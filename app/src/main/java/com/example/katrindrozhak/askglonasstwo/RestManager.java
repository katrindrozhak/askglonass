package com.example.katrindrozhak.askglonasstwo;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// для создания ретрофита и сервиса
public class RestManager {
    private final String BASE_URL = "http://195.93.229.66:4242/";
    private Api server;

    public Api getServerApi() {
        if (server == null) {

            // Создадаем инстанс Retrofit,
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            //создадим наш сервис.
            server = retrofit.create(Api.class);
        }
        return server;
    }
}
