package com.example.katrindrozhak.askglonasstwo;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

// создаем интерфейс
// с GET запросом
public interface Api {

    @GET("/main")
    Call<JsonSchemaInfo> getData(@QueryMap Map<String, String> options);
}
