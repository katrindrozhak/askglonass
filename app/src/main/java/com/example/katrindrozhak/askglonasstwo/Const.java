package com.example.katrindrozhak.askglonasstwo;

public class Const {
    public static final String FUNK_KEY   = "func";
    public static final String FUNK_VALUE = "state";
    public static final String KEY_FUEL   = "fuel";
    public static final String VALUE_FUEL = "true";
    public static final String UID_KEY    = "uid";
    public static final String UID_VALUE  = "d8f9e2b6-678d-4036-ae31-9e7967d2987f";
    public static final String OUT_KEY    = "out";
    public static final String OUT_VALUE  = "json";
}
